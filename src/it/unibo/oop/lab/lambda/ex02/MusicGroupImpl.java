package it.unibo.oop.lab.lambda.ex02;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

import javax.print.DocFlavor.STRING;
import javax.swing.plaf.OptionPaneUI;

/**
 *
 */
public class MusicGroupImpl implements MusicGroup {

    private final Map<String, Integer> albums = new HashMap<>();
    private final Set<Song> songs = new HashSet<>();

    @Override
    public void addAlbum(final String albumName, final int year) {
        this.albums.put(albumName, year);
    }

    @Override
    public void addSong(final String songName, final Optional<String> albumName, final double duration) {
        if (albumName.isPresent() && !this.albums.containsKey(albumName.get())) {
            throw new IllegalArgumentException("invalid album name");
        }
        this.songs.add(new MusicGroupImpl.Song(songName, albumName, duration));
    }

    @Override
    public Stream<String> orderedSongNames() {
        final Set<String> orderedSong = new HashSet<>();
        songs.forEach(t -> orderedSong.add(t.getSongName()));
        return orderedSong.stream().sorted();
    }

    @Override
    public Stream<String> albumNames() {
        final Set<String> albumNames = new HashSet<>();
        this.albums.forEach((t,k) -> albumNames.add(t));
    	return albumNames.stream();
    }

    @Override
    public Stream<String> albumInYear(final int year) {
        final Set<String> albumInYear = new HashSet<>();
        this.albums.forEach((t,k) -> {
        	if(k == year) {
        		albumInYear.add(t);
        	}
        });
    	return albumInYear.stream();
    }

    @Override
    public int countSongs(final String albumName) {
    	return songs.stream()
    			.filter(p -> p.getAlbumName().isPresent())
    			.filter(p -> p.getAlbumName().get().equals(albumName))
    			.mapToInt(p->1)
    			.sum();
    }

    @Override
    public int countSongsInNoAlbum() {
        return songs.stream()
        		.filter(p -> !p.getAlbumName().isPresent())
        		.mapToInt(p -> 1)
        		.sum();
    }

    @Override
    public OptionalDouble averageDurationOfSongs(final String albumName) {
    	return songs.stream()
    			.filter(p -> p.getAlbumName().isPresent())
    			.filter(p -> p.getAlbumName().get().equals(albumName))
    			.mapToDouble(p -> p.duration)
    			.average();
    }

    @Override
    public Optional<String> longestSong() {
    	double max = songs.stream().mapToDouble(t -> t.getDuration()).max().getAsDouble();
    	return Optional.ofNullable(songs.stream()
    			.filter(p -> p.getDuration() == max)
    			.findFirst()
    			.get()
    			.getSongName());
    	
    	
//    	Optional<String> name = Optional.empty();
//    	for (Song s: songs) {
//    		if(s.getDuration() == max) {
//    			name = Optional.ofNullable(s.getSongName());
//    		}
//    	}
//        return name;

    }

    @Override
    public Optional<String> longestAlbum() {
    	Set<String> set = albums.keySet();
    	String maxName = null;
    	double maxDuration=0;
    	for(String album : set) {
    	    double sum = songs.stream().filter(p -> p.getAlbumName().isPresent()).filter(p -> p.getAlbumName().get().equals(album)).mapToDouble(p -> p.getDuration()).sum();
    	    if (maxDuration < sum ) {
    	    	maxName = album;
    	    	maxDuration = sum;
    	    }
    	}
    	
        return Optional.ofNullable(maxName);
    }

    private static final class Song {

        private final String songName;
        private final Optional<String> albumName;
        private final double duration;
        private int hash;

        Song(final String name, final Optional<String> album, final double len) {
            super();
            this.songName = name;
            this.albumName = album;
            this.duration = len;
        }

        public String getSongName() {
            return songName;
        }

        public Optional<String> getAlbumName() {
            return albumName;
        }

        public double getDuration() {
            return duration;
        }

        @Override
        public int hashCode() {
            if (hash == 0) {
                hash = songName.hashCode() ^ albumName.hashCode() ^ Double.hashCode(duration);
            }
            return hash;
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof Song) {
                final Song other = (Song) obj;
                return albumName.equals(other.albumName) && songName.equals(other.songName)
                        && duration == other.duration;
            }
            return false;
        }

        @Override
        public String toString() {
            return "Song [songName=" + songName + ", albumName=" + albumName + ", duration=" + duration + "]";
        }

    }

}
